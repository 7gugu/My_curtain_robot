#include "wakeup.h"
#include "stdio.h"
#include "string.h"
#include "gpio.h"

uint8_t gpio_flag= 0;
uint8_t Sleep_Flag =0;
uint16_t Calibration_Times_Flag = 0;

void SystemPower_Config(void)    //配置低功耗模式  包括GPIO配置等。。。
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    /* Enable Power Control clock */
    __HAL_RCC_PWR_CLK_ENABLE();
    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
    /* Enable Ultra low power mode */
    HAL_PWREx_EnableUltraLowPower();

    /* Enable the fast wake up from Ultra low power mode */
    HAL_PWREx_EnableFastWakeUp();
	
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();

    //串口2接收中断改为外部中断便于唤醒！！
    GPIO_InitStruct.Pin = GPIO_PIN_3;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;  // 输入 上升沿触发
    GPIO_InitStruct.Pull = GPIO_NOPULL; //GPIO_NOPULL  GPIO_PULLDOWN  //下拉   
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
    HAL_NVIC_SetPriority(EXTI2_3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI2_3_IRQn);
	
	//其他无用IO 配置为模拟输入
 /*Configure GPIO pins : PA9 PA10  */
//  GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_10;
//  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
//  GPIO_InitStruct.Pull = GPIO_NOPULL;
//  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

//    /* Disable GPIOs clock */
//    __HAL_RCC_GPIOA_CLK_DISABLE();
//    __HAL_RCC_GPIOB_CLK_DISABLE();
//    __HAL_RCC_GPIOC_CLK_DISABLE();

  printf("Will to sleep......\r\n");
}

void SystemClockConfig_STOP(void)   //恢复外设时钟
{

    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
    RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

    /** Configure the main internal regulator output voltage
    */
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
    /** Initializes the CPU, AHB and APB busses clocks
    */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
    RCC_OscInitStruct.LSIState = RCC_LSI_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }
    /** Initializes the CPU, AHB and APB busses clocks
    */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                                  |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
    {
        Error_Handler();
    }
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2
                                         |RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_RTC;
    PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
    PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
    PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
    PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
    {
        Error_Handler();
    }
}
//void RTC_Time_Config(uint32_t stoptime)
//{
//    printf("into_sleep_mode\r\nwill sleep %d s\r\n",stoptime);
//    if(HAL_RTCEx_DeactivateWakeUpTimer(&hrtc) != HAL_OK)
//    {
//        /*Initialization Error*/
//        Error_Handler();
//    }
//    /*## Setting the Wake up time ############################################*/
//    HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, stoptime, RTC_WAKEUPCLOCK_CK_SPRE_16BITS);  //配置为20S

//}
//低功耗任务
void LP_Task(void)
{
    if(Sleep_Flag == 1)
    {
        Sleep_Flag =0;
        /*不可将NC200休眠，若休眠则无法使用地磁检测也进入不了中断，但是功耗可以稳定在80微安。
          如果NC200不休眠的话，功耗会80-300 之间跳变，待解决。
        */
        HAL_Delay(1000);
        NVIC_SystemReset();           //主动复位，进入休眠
    }
}

//void reset_wake_up(void)
//{
//	uint32_t E2P_Val = 0;
//    if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST) != RESET)  //看门狗复位
//    {
//        printf("@@ dog_rst!...\r\n");
//        __HAL_RCC_CLEAR_RESET_FLAGS();
//        MX_IWDG_Init();  //初始化看门狗
//		   if(WeakUP_flag==0)
//        {
//            NB_Init(0);                //NB初始化
//			EEPROM_Read_Word(NC200_ADDR,&E2P_Val);
//            if(E2P_Val == Clear_Data) //地磁还未初始化
//            {
//                Magnetic_init(0);     //地磁初始化
//            } else {
//                Magnetic_init(1);     //地磁初始化
//            }
//        }
//    }
//    else  //初次开机或者用户主动复位或者休眠复位
//    {
//        printf("@@ per_rst!...\r\n");
//		EEPROM_Read_Word(REST_ADDR,&E2P_Val);
//        if(E2P_Val == REST_ADDR_Data)   //具有休眠标识
//        {
//			printf("****************");
//            EEPROM_Write_Word(REST_ADDR,Clear_Data);     //写入清空休眠标识
//			printf("即将进入休眠....\r\n");
//            SystemPower_Config();            //配置低功耗模式（GPIO相关）
//			RTC_Time = atoi((char *)e2p_argument.SERVER_KEEP_ALIVE)*60;
//            RTC_Time_Config(RTC_Time);		 //配置RTC时钟   可以实现定时主动唤醒进行自检  RTC_Time
//            HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI); //进入STOP低功耗模式
//            //从这里唤醒
//            SystemClockConfig_STOP();    //恢复外设时钟
//            MX_IWDG_Init();  //初始化看门狗
//            Calibration_Times_Flag ++;
//            if (__HAL_PWR_GET_FLAG(PWR_FLAG_WU) != RESET)  //判断是何种情况唤醒
//            {
//                printf("@@ weakup!...\r\n");
//                WeakUP_flag = 1;   //
//            } else {
//                printf("@@ Other weakUP!...\r\n");
//                WeakUP_flag = 1;   //
//            }
//            if(Calibration_Times_Flag>(24*60*60/RTC_Time))   // 12小时对表一次
//            {
//                printf("联网对表...\r\n");
//                Calibration_Times_Flag = 0;
//                Cat_Time.flag = 0;
//            }
//        }
//		else  //没有休眠标识
//        {
//            printf("@@ 不想休眠...\r\n");
//            MX_IWDG_Init();  //初始化看门狗
//			if(WeakUP_flag==0)
//            {
//                NB_Init(0);          //NB初始化
//			EEPROM_Read_Word(NC200_ADDR,&E2P_Val);
//                if(E2P_Val== Clear_Data) //地磁还未初始化
//                {
//                    Magnetic_init(0);    //地磁初始化
//                } else {
//                    Magnetic_init(1);    //地磁初始化
//                }
//            }
//        }
//    }
////判断是否看门狗复位   Tout=((4×2^PRER) ×RLR)/LSI时钟频率
//}


